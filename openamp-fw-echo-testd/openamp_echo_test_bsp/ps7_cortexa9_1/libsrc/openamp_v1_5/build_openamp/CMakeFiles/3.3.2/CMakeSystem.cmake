set(CMAKE_HOST_SYSTEM "Linux-5.3.0-45-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "5.3.0-45-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/opt/development/zynq/zed-ChallengeX/build/tmp/work/plnx_zynq7-xilinx-linux-gnueabi/openamp-fw-echo-testd/2019.2+gitAUTOINC+e8db5fb118-r0/build/openamp-fw-echo-testd/openamp_echo_test_bsp/ps7_cortexa9_1/libsrc/openamp_v1_5/src/open-amp/cmake/platforms/toolchain.cmake")

set(CMAKE_SYSTEM "FreeRTOS")
set(CMAKE_SYSTEM_NAME "FreeRTOS")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "arm")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
